"Resource\ContentControlDialog.r"
{
	"ContentControlDialog"
	{
		"ControlName"		"Frame"
		"fieldName"		"ContentControlDialog"
		"xpos"		"158"
		"ypos"		"197"
		"wide"		"328"
		"tall"		"250"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
	}
	"ContentStatus"
	{
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"10000"
		"ypos"			"10000"
	}
	"PasswordPrompt"
	{
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"10000"
		"ypos"			"10000"
	}
	"PasswordReentryPrompt"
	{
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"10000"
		"ypos"			"10000"
	}
	"ContentControlExplain"
	{
		"ControlName"		"Label"
		"fieldName"		"ContentControlExplain"
		"xpos"		"30"
		"ypos"		"182"
		"wide"		"272"
		"tall"		"28"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
		"labelText"		"If you're seeing this, then you broke the UI somehow! Good job."
		"textAlignment"		"west"
		"dulltext"		"0"
	}
	"Password"
	{
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"10000"
		"ypos"			"10000"
	}
	"Password2"
	{
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"10000"
		"ypos"			"10000"
	}
	"Ok"
	{
		"visible"		"1"
		"enabled"		"1"
		"xpos"			"10000"
		"ypos"			"10000"
	}
	"Cancel"
	{
		"ControlName"		"Button"
		"fieldName"		"Cancel"
		"xpos"		"248"
		"ypos"		"216"
		"wide"		"72"
		"tall"		"24"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"4"
		"labelText"		"#GameUI_Cancel"
		"textAlignment"		"west"
		"dulltext"		"0"
		"command"		"Cancel"
		"default"		"0"
	}
	"BuildDialog"
	{
		"ControlName"		"BuildModeDialog"
		"fieldName"		"BuildDialog"
		"xpos"		"504"
		"ypos"		"51"
		"wide"		"285"
		"tall"		"590"
		"autoResize"		"0"
		"pinCorner"		"0"
		"visible"		"1"
		"enabled"		"1"
		"tabPosition"		"0"
	}
}
