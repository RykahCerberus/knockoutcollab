500

//This file is used for weapon hud sprites instead of the original weapon_(name).txt files. This is so hud sprites can be changed.

weapon_9mmAR				640	640hud1_LD		0	135	170	45
weapon_9mmAR_s				640	640hud4_LD		0	135	170	45
weapon_9mmAR_ammo			640	640hud7			0	72	24	24
weapon_9mmAR_ammo2			640	640hud7			48	72	24	24

weapon_9mmAR_hd				640	640hud1			0	135	170	45
weapon_9mmAR_hd_s			640	640hud4			0	135	170	45
weapon_9mmAR_hd_ammo		640	640hud7			0	72	24	24
weapon_9mmAR_hd_ammo2		640	640hud7			48	72	24	24

weapon_9mmhandgun			640	640hud1_LD		0	45	170	45
weapon_9mmhandgun_s			640	640hud4_LD		0	45	170	45
weapon_9mmhandgun_ammo		640	640hud7			0	72	24	24
weapon_9mmhandgun_ammo2		640	invisible 		0 	0 	0	0

weapon_9mmhandgun_hd		640	640hud1			0	45	170	45
weapon_9mmhandgun_hd_s		640	640hud4			0	45	170	45
weapon_9mmhandgun_hd_ammo	640	640hud7			0	72	24	24
weapon_9mmhandgun_hd_ammo2	640	invisible 		0 	0 	0	0

weapon_357					640	640hud1_LD		0	90	170	45
weapon_357_s				640	640hud4_LD		0	90	170	45
weapon_357_ammo				640	640hud7			24	72	24	24
weapon_357_ammo2			640	invisible 		0 	0 	0	0

weapon_357_hd				640	640hud1			0	90	170	45
weapon_357_hd_s				640	640hud4			0	90	170	45
weapon_357_hd_ammo			640	640hud7			24	72	24	24
weapon_357_hd_ammo2			640	invisible 		0 	0 	0	0

weapon_crossbow				640	640hud2			0	0	170	45
weapon_crossbow_s			640	640hud5			0	0	170	45
weapon_crossbow_ammo		640	640hud7			96	72	24	24
weapon_crossbow_ammo2		640	invisible 		0 	0 	0	0

weapon_crowbar				640	640hud1_LD		0	0	170	45
weapon_crowbar_s			640	640hud4_LD		0	0	170	45
weapon_crowbar_ammo			640	invisible 		0 	0 	0	0
weapon_crowbar_ammo2		640	invisible 		0 	0 	0	0

weapon_crowbar_hd			640	640hud1			0	0	170	45
weapon_crowbar_hd_s			640	640hud4			0	0	170	45
weapon_crowbar_hd_ammo		640	invisible 		0 	0 	0	0
weapon_crowbar_hd_ammo2		640	invisible 		0 	0 	0	0

weapon_displacer			640	640hudof01 		0   180 170 45
weapon_displacer_s			640	640hudof02 		0   180 170 45
weapon_displacer_ammo		640	640hud7			0	96	24	24
weapon_displacer_ammo2		640	invisible 		0 	0 	0	0

weapon_eagle				640	640hudof01 		0   90  170 45
weapon_eagle_s				640	640hudof02 		0   90  170 45
weapon_eagle_ammo			640	640hud7 		24  72  24  24
weapon_eagle_ammo2			640	invisible 		0 	0 	0	0

weapon_egon					640	640hud2			0	135	170	45
weapon_egon_s				640	640hud5			0	135	170	45
weapon_egon_ammo			640	640hud7			0	96	24	24
weapon_egon_ammo2			640	invisible 		0 	0 	0	0

weapon_gauss				640	640hud2			0	90	170	45
weapon_gauss_s				640	640hud5			0	90	170	45
weapon_gauss_ammo			640	640hud7			0	96	24	24
weapon_gauss_ammo2			640	invisible 		0 	0 	0	0

weapon_grapple      	    640	640hudof01 		0   45  170 45
weapon_grapple_s	        640	640hudof02 		0   45  170 45
weapon_grapple_ammo			640	invisible 		0 	0 	0	0
weapon_grapple_ammo2		640	invisible 		0 	0 	0	0

weapon_handgrenade			640	640hud3			0	0	170	45
weapon_handgrenade_s		640	640hud6			0	0	170	45
weapon_handgrenade_ammo		640	640hud7			48	96	24	24
weapon_handgrenade_ammo2	640	invisible 		0 	0 	0	0

weapon_hornetgun			640	640hud2			0	180	170	45
weapon_hornetgun_s			640	640hud5			0	180	170	45
weapon_hornetgun_ammo		640	640hud7			24	96	24	24
weapon_hornetgun_ammo2		640	invisible 		0 	0 	0	0

weapon_knife         	 	640	640hudof03 		0   90  170 45
weapon_knife_s        		640	640hudof04 		0   90  170 45
weapon_knife_ammo			640	invisible 		0 	0 	0	0
weapon_knife_ammo2			640	invisible 		0 	0 	0	0

weapon_m249					640	640hudof01 		0   135 170 45
weapon_m249_s				640	640hudof02 		0   135 170 45
weapon_m249_ammo            640	640hud7 		24  72  24  24
weapon_m249_ammo2			640	invisible 		0 	0 	0	0

weapon_penguin				640	640hudof03		0	180	170	45
weapon_penguin_s			640	640hudof04		0	180	170	45
weapon_penguin_ammo			640	640hud7			144	72	24	24
weapon_penguin_ammo2		640	invisible 		0 	0 	0	0

weapon_pipewrench          	640	640hudof01 		0   0   170 45
weapon_pipewrench_s        	640	640hudof02 		0   0   170 45
weapon_pipewrench_ammo		640	invisible 		0 	0 	0	0
weapon_pipewrench_ammo2		640	invisible 		0 	0 	0	0

weapon_rpg					640	640hud2			0	45	170	45
weapon_rpg_s				640	640hud5			0	45	170	45
weapon_rpg_ammo				640	640hud7			120	72	24	24
weapon_rpg_ammo2			640	invisible 		0 	0 	0	0

weapon_satchel				640	640hud3			0	45	170	45
weapon_satchel_s			640	640hud6			0	45	170	45
weapon_satchel_ammo			640	640hud7			72	96	24	24
weapon_satchel_ammo2		640	invisible 		0 	0 	0	0

weapon_shockrifle			640	640hudof03 		0   45  170 45
weapon_shockrifle_s			640	640hudof04 		0   45  170 45
weapon_shockrifle_ammo		640	640hud7 		224 48  24  24
weapon_shockrifle_ammo2		640	invisible 		0 	0 	0	0

weapon_shotgun				640	640hud1_LD		0	180	170	45
weapon_shotgun_s			640	640hud4_LD		0	180	170	45
weapon_shotgun_ammo			640	640hud7			72	72	24	24
weapon_shotgun_ammo2		640	invisible 		0 	0 	0	0

weapon_shotgun_hd			640	640hud1			0	180	170	45
weapon_shotgun_hd_s			640	640hud4			0	180	170	45
weapon_shotgun_hd_ammo		640	640hud7			72	72	24	24
weapon_shotgun_hd_ammo2		640	invisible 		0 	0 	0	0

weapon_snark				640	640hud3			0	135	170	45
weapon_snark_s				640	640hud6			0	135	170	45
weapon_snark_ammo			640	640hud7			96	96	24	24
weapon_snark_ammo2			640	invisible 		0 	0 	0	0

weapon_sniperrifle          640	640hudof03 		0   135 170 45
weapon_sniperrifle_s        640	640hudof04 		0   135 170 45
weapon_sniperrifle_ammo		640	640hud7 		24  72  24  24
weapon_sniperrifle_ammo2	640	invisible 		0 	0 	0	0

weapon_sporelauncher		640	640hudof03 		0   0   170 45
weapon_sporelauncher_s		640	640hudof04 		0   0   170 45
weapon_sporelauncher_ammo	640	640hud7 		200 48  24  24
weapon_sporelauncher_ammo2	640	invisible 		0 	0 	0	0

weapon_tripmine				640	640hud3			0	90	170	45
weapon_tripmine_s			640	640hud6			0	90	170	45
weapon_tripmine_ammo		640	640hud7			120	96	24	24
weapon_tripmine_ammo2		640	invisible 		0 	0 	0	0

cs_weapon_m3				640 640CShud1		0	135	170	45
cs_weapon_m3_s				640 640CShud4		0	135	170	45
cs_weapon_m3_ammo			640 640CShud7		0	72	24	24
cs_weapon_m3_ammo2			640	invisible 		0 	0 	0	0

cs_weapon_glock18			640 640CShud1		0	45	170	45
cs_weapon_glock18_s			640 640CShud4		0	45	170	45
cs_weapon_glock18_ammo		640 640CShud7		48	72	24	24
cs_weapon_glock18_ammo2		640	invisible 		0 	0 	0	0

cs_weapon_c4				640 640CShud1		0	0	170	45
cs_weapon_c4_s				640 640CShud4		0	0	170	45
cs_weapon_c4_ammo			640 640CShud7		96	96	24	24
cs_weapon_c4_ammo2			640	invisible 		0 	0 	0	0

cs_weapon_ak47				640 640CShud10		0	0	170	45
cs_weapon_ak47_s			640 640CShud11		0	0	170	45
cs_weapon_ak47_ammo			640 640CShud7		72	72	24	24
cs_weapon_ak47_ammo2		640	invisible 		0 	0 	0	0

cs_weapon_deagle			640 640CShud10		0	90  170	45
cs_weapon_deagle_s			640 640CShud11		0	90	170	45
cs_weapon_deagle_ammo		640 640CShud7		24	72	24	24
cs_weapon_deagle_ammo2		640	invisible 		0 	0 	0	0

cs_weapon_knife				640 640CShud10		0	135	170	45
cs_weapon_knife_s			640 640CShud11		0	135	170	45
cs_weapon_knife_ammo		640	invisible 		0 	0 	0	0
cs_weapon_knife_ammo2		640	invisible 		0 	0 	0	0

cs_weapon_m249				640 640CShud3		0	0	170	45
cs_weapon_m249_s			640 640CShud3		0	0	170	45
cs_weapon_m249_ammo			640 640CShud7		0	96	24	24
cs_weapon_m249_ammo2		640	invisible 		0 	0 	0	0

cs_weapon_hegrenade			640 640CShud3		0	45	170	45
cs_weapon_hegrenade_s		640 640CShud3		0	45	170	45
cs_weapon_hegrenade_ammo	640 640CShud7		72	96	24	24
cs_weapon_hegrenade_ammo2	640	invisible 		0 	0 	0	0

//put your own icons below this and mark who added it


